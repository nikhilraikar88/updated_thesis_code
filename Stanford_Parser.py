from itertools import combinations
import gensim
from gensim.models import Word2Vec
from nltk.parse.stanford import StanfordDependencyParser
import os
from itertools import combinations



def process(S1,S2,l1,l2):
    if len(S1.intersection(S2)) == 0:
        print('There are no direct Objects')
        print(set(l1 + l2))
        # further_processing.append(list1[k], list1[k + 1])
    else:
        print(" ")
        if (len(l2) >= l1)):
            print(l2, 'REQUIRE', l1)
        else:
            print(l1, 'REQUIRE', l2)



def crossvalidate():
    #Converts a list into combinations
    list1=[]
    for elements in list(combinations(words, 2)):
        for i in elements:
            list1.append(i.split())
    k=0
    further_processing = []
    while k < len(list1):
        print(list1)
        model = gensim.models.KeyedVectors.load_word2vec_format('GoogleNews-vectors-negative300.bin', binary=True,limit=900000)
        sim = model.n_similarity(list1[k],list1[k+1])
        if(sim>0.20):
            print("The similarity threshold is more for this")
            print(list1[k],list1[k+1])
            print(sim)
            print(" ")
            print(list1[k])
            print(list1[k+1])
            print(" ")
            S1 = set(list1[k])
            print(S1)
            S2 = set(list1[k+1])
            print(S2)
            process(S1,S2,list1[k],list1[k+1])
            print(" ")
        k=k+2


# os.environ['JAVAHOME'] = 'C://Program Files//Java//jdk1.8.0_144//bin//java.exe'
# os.environ['STANFORD_PARSER'] = 'stanford-parser-full-2018-10-17/stanford-parser.jar'
# os.environ['STANFORD_MODELS'] = 'stanford-parser-full-2018-10-17/stanford-parser-3.9.2-models.jar'
# path_to_jar = 'stanford-parser-full-2018-10-17/stanford-parser.jar'
# path_to_models_jar = 'stanford-parser-full-2018-10-17/stanford-parser-3.9.2-models.jar'
# dep_parser = StanfordDependencyParser(path_to_jar=path_to_jar, path_to_models_jar=path_to_models_jar)
# list_process = []
# dobj1 = []
# dobj2 = []
# dobj_diff = []
#
# for i in list(further_processing):
#     further_processing.remove(i)
#     list_process.append(i)
#     print(list_process)
#     for z in list_process:
#         for s in sentences:
#             if (z[0]) in s:
#                 print(s)
#                 res = dep_parser.raw_parse(s)
#                 dep = list(res)[0]
#                 for triple in dep.triples():
#                     if (triple[1] == 'dobj'):
#                         print(triple[1], "(", triple[0][0], ", ", triple[2][0], ")")
#                         d1 = ' '.join(triple[0][0])
#                         d2 = ' '.join(triple[2][0])
#                         dobj1.append([d1, d2])
#                 if (len(dobj1) == 0):
#                     print(z[0], 'EXCLUDES', z[1])
#
#             if (z[1]) in s:
#                 print(s)
#                 res = dep_parser.raw_parse(s)
#                 dep = list(res)[0]
#                 for triple in dep.triples():
#                     if (triple[1] == 'dobj'):
#                         print(triple[1], "(", triple[0][0], ", ", triple[2][0], ")")
#                         d3 = ' '.join(triple[0][0])
#                         d4 = ' '.join(triple[2][0])
#                         dobj2.append([d3, d4])
#
#                 if (len(dobj2) == 0):
#                     print(z[0], 'EXCLUDES', z[1])
#
#             for element in dobj1:
#                 if element in dobj2:
#                     dobj_diff.append(element)
#                     print(len(dobj_diff))
#
#         if len(dobj_diff) == 0:
#             print(z[0], 'EXCLUDES', z[1])
#         else:
#             print(z[0], 'REQUIRE', z[1])
#
#         list_process = []
#         dobj2 = []
#         dobj1 = []
#         dobj_diff = []

if __name__ == "__main__":

    # List of features which are termed as DIRECT OBJECTS:
    words = ['alarm system', 'interior mirror', 'central locking system', 'automatic power window','manual power window',
             'power window','finger protection', 'exterior monitoring', 'LED alarm system', 'central alarm system']

    crosstree = crossvalidate()

    print(crosstree)
