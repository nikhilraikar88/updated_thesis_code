import gensim
from gensim.models import Word2Vec
from nltk.parse.stanford import StanfordDependencyParser
import os
from itertools import combinations
import numpy as np
import spacy
nlp = spacy.load('en')
import pandas as pd


data = pd.read_csv("BCS.csv")

# Define all the lists which needs to be used.
list1 = []
csv = []
list_process = []
dobj1 = []
dobj2 = []
dobj_diff = []

# Method 2
# where we make use of dependencies such as agentive,action and objective to achieve our tasks
a = []
b = []
d = []
e = []
diff = []

# List of features which are termed as DIRECT OBJECTS:
words = ['alarm system', 'interior mirror', 'central locking system', 'automatic power window', 'manual power window',
         'power window','finger protection', 'exterior monitoring', 'LED alarm system', 'central alarm system']


# Converts a list into combinations.
for elements in list(combinations(words, 2)):
    for i in elements:
        list1.append(i.split())

#
k = 0
further_processing = []
def RelationshipExtraction(model):
    global k
    while k < len(list1):
        sim = model.n_similarity(list1[k], list1[k + 1])  # Check Similarity!
        if (sim > 0.40):  # Similarity Threshold
            print("The similarity threshold is more for this")
            print(list1[k], list1[k + 1])
            print('Similarity:', sim)
            print(" ")
            S1 = set(list1[k])
            S2 = set(list1[k + 1])
            CrossValidate(S1, S2, k)
            print(" ")
        k = k + 2


def CrossValidate(S1, S2, k):
    if len(S1.intersection(S2)) == 0:
        print('There are no direct Objects')
        print(" ")
        print(list1[k], list1[k + 1])
        a = ' '.join(list1[k])
        b = ' '.join(list1[k + 1])
        further_processing.append([a, b])
    else:
        print(" ")
        Result(k)


def Result(k):
    if (len(list1[k + 1]) >= len(list1[k])):
        print(list1[k + 1], 'REQUIRE', list1[k])
        ex1 = ' '.join(list1[k])
        ex2 = ' '.join(list1[k + 1])
        csv.append([ex1, 'REQUIRE', ex2])

    else:
        print(list1[k], 'REQUIRE', list1[k + 1])
        ex3 = ' '.join(list1[k])
        ex4 = ' '.join(list1[k + 1])
        csv.append([ex3, 'REQUIRE', ex4])


def dobjExtract():
    global i, list_process, dobj2, dobj1, dobj_diff
    for i in list(further_processing):
        further_processing.remove(i)
        list_process.append(i)
        print(list_process)
        ExtractDobj()


def ExtractDobj():
    global list_process, dobj2, dobj1, dobj_diff
    for z in list_process:
        ValidateDobj(dobj1, dobj2, dobj_diff, z)

        list_process = []
        dobj2 = []
        dobj1 = []
        dobj_diff = []


def ValidateDobj(dobj1, dobj2, dobj_diff, z):
    CompareDobj(dobj1, dobj2, dobj_diff, z)
    if len(dobj_diff) == 0:
        print(z[0], 'EXCLUDES', z[1])
        csv.append([z[0], 'EXCLUDES', z[1]])
    else:
        csv.append([z[0], 'REQUIRE', z[1]])
        print(z[0], 'REQUIRE', z[1])


def CompareDobj(dobj1, dobj2, dobj_diff, z):
    for col in data['FeatureSet']:
        if (z[0]) in col:
            res1 = nlp(col)
            dobj1Extract(dobj1, res1, z)

        if (z[1]) in col:
            res2 = nlp(col)
            dobj2Extract(dobj2, res2)

            if (len(dobj2) == 0):
                print(z[0], 'EXCLUDES', z[1])

        for element in dobj1:
            if element in dobj2:
                dobj_diff.append(element)
                print(len(dobj_diff))


def dobj2Extract(dobj2, res2):
    for child2 in res2:
        if (child2.dep_ == 'dobj'):
            dobj2.append(child2)


def dobj1Extract(dobj1, res1, z):
    for child1 in res1:
        if (child1.dep_ == 'dobj'):
            dobj1.append(child1)
    if (len(dobj1) == 0):
        print(z[0], 'EXCLUDES', z[1])


def CasesExtraction():
    global i, z, list_process, b, d, diff
    ExtractCases(d, z)
    AppendResultKeyone(b)
    AppendResultKeytwo(d)
    CheckList(b, diff)
    ExtractResult(z)
    list_process = []
    b = []
    d = []
    diff = []


def ExtractResult(z):
    if len(dobj_diff) == 0:
        print(z[0], 'EXCLUDES', z[1])
    else:
        print(z[0], 'REQUIRE', z[1])


def CheckList(b, diff):
    for element in b:
        if element in e:
            diff.append(element)
            print(len(diff))


def AppendResultKeytwo(d):
    for j in d:
        for g in j:
            e.append(g)


def AppendResultKeyone(b):
    global i, z
    for i in a:
        for z in i:
            b.append(z)


def ExtractCases(d, z):
    for col in data['FeatureSet']:
        if (z[0]) in col:
            print(col)
            a1, a2, a4 = KeywordOneCaseExtract()

        if (z[1]) in col:
            print(col)
            for triple in dep.triples():
                KeywordtwoCaseExtract(a1, a2, a4, d, triple)


def KeywordtwoCaseExtract(a1, a2, a4, d, triple):
    if (triple[1] == 'nsubj'):
        print('The Agentive is:', triple[2][0])
        d1 = ' '.join(triple[2][0])
        print('The Action is:', triple[0][0])
        d2 = ' '.join(triple[0][0])
        a.append([a1, a2])
    if (triple[1] == 'advcl'):
        print('The Goal is:', triple[2][0])
        d4 = triple[2][0]
        d3 = ' '.join(triple[2][0])
    if triple[1] == 'dobj' and triple[0][0] == a4:
        print('The goal of the sentence is:', a4, triple[2][0])
        d5 = a4 + " " + triple[2][0]
        d.append([d5])
        print(d5)


def KeywordOneCaseExtract():
    for triple in dep.triples():
        if (triple[1] == 'nsubj'):
            print('The Agentive is:', triple[2][0])
            a1 = ' '.join(triple[2][0])
            print('The Action is:', triple[0][0])
            a2 = ' '.join(triple[0][0])
            a.append([a1, a2])
        if (triple[1] == 'advcl'):
            print('The Goal is:', triple[2][0])
            a4 = triple[2][0]
            a3 = ' '.join(triple[2][0])
        if triple[1] == 'dobj' and triple[0][0] == a4:
            print('The goal of the sentence is:', a4, triple[2][0])
            a5 = a4 + " " + triple[2][0]
            a.append([a5])
            print(a5)
    return a1, a2, a4


def CaseExtraction():
    global i, z
    for i in list(further_processing):
        further_processing.remove(i)
        list_process.append(i)
        print(list_process)
        for z in list_process:
            CasesExtraction()




if __name__ == "__main__":
    model = gensim.models.KeyedVectors.load_word2vec_format('GoogleNews-vectors-negative300.bin', binary=True,limit=90000)
    RelationshipExtraction(model)
    dobjExtract()
    Final_data = pd.DataFrame(csv)
    Final_data.to_csv('results_exclude_require.csv')
    # Method2 which contains Cases and Extraction Process of those cases.
    CaseExtraction()