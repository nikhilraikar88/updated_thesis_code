# from gensim.test.utils import common_texts, get_tmpfile
#
# from gensim.models import Word2Vec
#
# import gensim
#
# model = gensim.models.KeyedVectors.load_word2vec_format('german.model', binary=True)
#
# print(model.vocab.keys())

import pickle
import random
import nltk
from ClassifierBasedGermanTagger.ClassifierBasedGermanTagger import ClassifierBasedGermanTagger

with open('nltk_german_classifier_data.pickle', 'rb') as f:
    tagger = pickle.load(f)

print(tagger.tag(['Das', 'ist', 'ein', 'einfacher', 'Test']))



